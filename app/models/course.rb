class Course < ActiveRecord::Base

  has_one :prerequisite, dependent: :destroy
  belongs_to :program


def self.search(search)
  if search
    where('name LIKE ?', "%#{search}%")
  else
    all
  end
end


  
end
