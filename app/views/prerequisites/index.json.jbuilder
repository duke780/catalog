json.array!(@prerequisites) do |prerequisite|
  json.extract! prerequisite, :id, :operator, :operandA, :operandB
  json.url prerequisite_url(prerequisite, format: :json)
end
