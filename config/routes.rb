Rails.application.routes.draw do

  root                              'home#index'

  get   'about'                 =>  'home#about'
  get   'faq'                   =>  'home#faq'
  get   'contact'               =>  'contacts#new'

  resources :prerequisites
  resources :courses
  resources :contacts

end
