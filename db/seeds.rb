operators = {}
["and", "or", "min_grade"].each do |name|
  operators[name] = Operator.create(name: name)
end

grades = {}
["A+", "A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"].each do |name|
  grades[name] = Grade.create(name: name)
end

courses = {}
["CS 131L", "CS 132L", "CS 151L", "CS 152L", "CS 251L"].each do |name|
  courses[name] = Course.create(name: name, description: "A #{name.sub(/\s.*/, '')} course")
end

courses["CS 132L"].prerequisite = Prerequisite.create(
  operator: operators["min_grade"].id,
  operandA: courses["CS 131L"].id,
  operandB: grades["F"].id
)
courses["CS 132L"].save

courses["CS 251L"].prerequisite = Prerequisite.create(
  operator: operators["or"].id,
  operandA: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 151L"].id,
    operandB: grades["F"].id).id,
  operandB: Prerequisite.create(
    operator: operators["min_grade"].id,
    operandA: courses["CS 152L"].id,
    operandB: grades["F"].id).id
)
courses["CS 251L"].save
